FROM alpine:3.17
MAINTAINER rleigh@codelibre.net

RUN apk update && apk add \
    build-base \
    cmake \
    musl-locales \
    readline-dev

ENV LC_ALL=en_US.UTF-8
ENV LANG=en_US.UTF-8
